ga('create', 'UA-165957691-1', 'auto');


ga('set', {
  userId: getParameterByName('userId'), 
  dimension1: '5943d4efa3d24b443f4008a2',
  dimension2: 'tenant',
  dimension3: '5943d4f0a3d24b443f4008d1'
});


ga('send', 'pageview');

console.log(ga.q, "GA queue");



/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

function getParameterByName(name, url) {
  if (!url) url = window.location.href;
  name = name.replace(/[\[\]]/g, '\\$&');
  var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
      results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, ' '));
}